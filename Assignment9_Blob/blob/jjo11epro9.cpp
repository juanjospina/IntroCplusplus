/***********************************************************************************************/
// Project name:    Recursive Blob Finder
// Author:		    Juan Jose Ospina Casas
// Email:			jjo11e@my.fsu.edu
// TA:			    Nan Zhao
// Course Info:	    COP3014
// Project Number:	9
// Due Date:	    08/02/2013
// Summary:		    The program counts and detects all the blobs in a txt file. Blobs are shown
//					as 'X's on the file. 
// Output:			Prints out the file itself with columns enumerated and the summary of where 
//					can be found each blob (starting 'X'). Prints out the number of blobs found.
// Assumptions:		The text file passed in must be called "blob.txt" - Requirement of hard code.
/***********************************************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

using namespace std;

// Constants
const int NUMROWS = 22;
const int NUMCOLUMNS = 72;
const int MAXSIZE = 80;

// Prototype
void BlobPath(char blob[][NUMCOLUMNS], int Row, int Col);

int main(void)
{
		ifstream mfile;
		int row = 0;
	    int column = 0;
		int blobcount = 0;
        char blob[NUMROWS][NUMCOLUMNS];

		mfile.open("blob.txt");

        cout << setw(10) << "1" << setw(10) << "2" << setw(10)
		<< "3" << setw(10) << "4" << setw(10) <<"5" << setw(10)
		<< "6" << setw(10) << "7" << endl;

        cout << "123456789012345678901234567890123456789";
        cout << "0123456789012345678901234567890" << endl;

		  for(int row = 0; row < NUMROWS; row++)
        {
                for (int column = 0; column < NUMCOLUMNS; column++)
                {
                        blob[row][column] = ' ';
                }
        }

		  for (int row = 1; row < NUMROWS-1; row++)
        {
                char lineinput[MAXSIZE];
                mfile.getline(lineinput, MAXSIZE);

                cout << lineinput << endl;

                for (int column = 1; column < NUMCOLUMNS-1; column++)
                {
                        blob[row][column] = lineinput[column-1];
                }
        }


        for (int row = 1; row < NUMROWS; row++)
        {
                for (int column = 1; column < NUMCOLUMNS; column++)
                {
                        if (blob[row][column] != ' ')
                        {
                               BlobPath(blob, row, column);

                                cout << "Blob at row: " << row;
                                cout << " column: " << column << endl;
                                blobcount++;
                        }
                }
        }

		cout << "Number of blobs: " << blobcount << endl;

        mfile.close();

		return 0;
}




// *********************************************************************
// * Member Function: BlobPath                                    
// * Description: Recursive function that navigates throughout and 
//				  locates the "path" of the blobs found on the txt file.
// * Parameter List: char blob[][] - The array where the info in gathered.
//					 int Row - integer indicating the row.
//					 int Col - integer indicating the column.
// * Author: Juan Jose Ospina                                     
// * Date: July 29, 2013                                               
// *********************************************************************

void BlobPath(char blob[][NUMCOLUMNS], int Row, int Col)
{
        blob[Row][Col] = ' ';

                if (blob[Row-1][Col+1] != ' ')
                {
                        BlobPath(blob, Row-1, Col+1);
                }
                if (blob[Row][Col+1] != ' ')
                {
                        BlobPath(blob, Row, Col+1);
                }
                if (blob[Row+1][Col+1] != ' ')
                {
                        BlobPath(blob, Row+1, Col+1);
                }
                if (blob[Row+1][Col] != ' ')
                {
                        BlobPath(blob, Row+1, Col);
                }
                if (blob[Row+1][Col-1] != ' ')
                {
                        BlobPath(blob, Row+1, Col-1);
                }
                if (blob[Row][Col-1] != ' ')
                {
                        BlobPath(blob, Row, Col-1);
                }
                if (blob[Row+1][Col-1] != ' ')
                {
                        BlobPath(blob, Row+1, Col-1);
                }
                if (blob[Row+1][Col] != ' ')
                {
                        BlobPath(blob, Row+1, Col);
                }
}
