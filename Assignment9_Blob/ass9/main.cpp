#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

using namespace std;

const int NUMROWS = 22;
const int NUMCOLUMNS = 72;
const int MAXSIZE = 80;


void BlobPath(char blob[][NUMCOLUMNS], int Row, int Col);

int main(void)
{
		ifstream mfile;
	//	int row = 0;
    //    int column = 0;
		int blobcount = 0;
        char blob[NUMROWS][NUMCOLUMNS];

		mfile.open("blob.txt");

        cout << setw(10) << "1" << setw(10) << "2" << setw(10)
		<< "3" << setw(10) << "4" << setw(10) <<"5" << setw(10)
		<< "6" << setw(10) << "7" << endl;

        cout << "123456789012345678901234567890123456789";
        cout << "0123456789012345678901234567890" << endl;

		  for(int row = 0; row < NUMROWS; row++)
        {
                for (int column = 0; column < NUMCOLUMNS; column++)
                {
                        blob[row][column] = ' ';
                }
        }

		  for (int row = 1; row < NUMROWS-1; row++)
        {
                char lineinput[MAXSIZE];
                mfile.getline(lineinput, MAXSIZE);

                cout << lineinput << endl;

                for (int column = 1; column < NUMCOLUMNS-1; column++)
                {
                        blob[row][column] = lineinput[column-1];
                }
        }


        for (int row = 1; row < NUMROWS; row++)
        {
                for (int column = 1; column < NUMCOLUMNS; column++)
                {
                        if (blob[row][column] != ' ')
                        {
                               BlobPath(blob, row, column);

                                cout << "Blob at row: " << row;
                                cout << " column: " << column << endl;
                                blobcount++;
                        }
                }
        }

		cout << "Number of blobs: " << blobcount << endl;



        mfile.close();


		return 0;
}





void BlobPath(char blob[][NUMCOLUMNS], int Row, int Col)
{
        blob[Row][Col] = ' ';

                if (blob[Row-1][Col+1] != ' ')
                {
                        BlobPath(blob, Row-1, Col+1);
                }
                if (blob[Row][Col+1] != ' ')
                {
                        BlobPath(blob, Row, Col+1);
                }
                if (blob[Row+1][Col+1] != ' ')
                {
                        BlobPath(blob, Row+1, Col+1);
                }
                if (blob[Row+1][Col] != ' ')
                {
                        BlobPath(blob, Row+1, Col);
                }
                if (blob[Row+1][Col-1] != ' ')
                {
                        BlobPath(blob, Row+1, Col-1);
                }
                if (blob[Row][Col-1] != ' ')
                {
                        BlobPath(blob, Row, Col-1);
                }
                if (blob[Row+1][Col-1] != ' ')
                {
                        BlobPath(blob, Row+1, Col-1);
                }
                if (blob[Row+1][Col] != ' ')
                {
                        BlobPath(blob, Row+1, Col);
                }
}
