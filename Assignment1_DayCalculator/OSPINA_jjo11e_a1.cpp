/*******************************************************************/
// Project name:	Day Calculator
// Author:			Juan Jose Ospina Casas
// Email:			jjo11e@my.fsu.edu
// TA:				Nan Zhao
// Course Info:     COP3014
// Project Number:  1
// Due Date:        05/24/2013
// Summary:			Find the specific day's name for the input given.
// Input:			3 numbers (day, month, year)
// Output:			The name of the specific day's name for the given 
//					user input.
// Assumptions:     All inputs should be valid.
/*******************************************************************/

#include <iostream>
#include <string>

using namespace std;

int main(void)
{
	// Array of Strings containing the names of the days.
	string Days[7]={"Sunday","Monday","Tuesday","Wednesday","Thursday",
					 "Friday","Saturday"};

    // Declaring the variables
	 int month,year,day;	     	 // Store variables used for calculations
	 int a,y,m,d;					// Intermediate variables used for calculations.

    // Get the number representing the Day.
	cout << "Please enter your Day (1-31): "<< endl;
	cin >> day;

	//Get the number representing the Month.
	cout << "Please enter your Month (1-12): " << endl;
    cin >> month;

	//Get the number representing the Year.
	cout << "Please enter your Year (1582 or later): " << endl;
    cin >> year;
	
	// Calculation (Algortihm) done in order to get the specific day.
	a = (14-month)/12;
	y=year-a;
	m = month+12*a-2;
	d = (day+y+y/4-y/100+y/400+(31*m/12))%7;

	// Display the result, Output.
	cout <<endl <<"The Date: "<<month<<"/"<<day<<"/"<<year
		<<" Falls on a: "<< Days[d]<<endl;

    return 0;
}
