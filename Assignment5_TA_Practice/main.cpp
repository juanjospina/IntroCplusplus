#include <iostream>
#include <string>

using namespace std;

const string LINE = "\n=======================================\n";

int main(void)
{
	// variable declaration
	int number;
	cout << LINE << "int number; " << LINE;
	cout << "ADDRESS of number (&number) is " << &number<< endl;
	cout << endl;

	number = 10;
	cout << LINE << "number = 10; " << LINE;
	cout << "VALUE of number is " << number << endl;
	cout << "ADDRESS of number (&number) is " << &number << endl;
	cout << endl;

	cout << LINE << "*x: x stores an address and what is stored in this address" << endl;
	cout << "&x: what is the address of x" << LINE;

	int* numberPtr;
	cout << LINE << "int* numberPtr; " << LINE;
	cout << "ADDRESS of numberPtr (&numberPtr) is " << &numberPtr<< endl;
	cout << endl;

	numberPtr = &number;
	cout << LINE << "numberPtr = &number; " << LINE;
	cout << "Value of numberPtr is " << numberPtr<< endl;
	cout << "Value of *numberPtr is " << *numberPtr<< endl;
	cout << "Value of &*numberPtr is " << &*numberPtr<< endl;
	cout << "ADDRESS of numberPtr (&numberPtr) is " << &numberPtr<< endl << endl;
	cout << "Value of *&numberPtr is " << *&numberPtr<< endl;
	cout << endl;


	int** numberPtrPtr;
	numberPtrPtr = &numberPtr;
	cout << LINE << "int** numberPtrPtr; " << endl;
	cout <<  "numberPtrPtr = &numberPtr; " << LINE;
	cout << "Now value of numberPtrPtr is " << numberPtrPtr << endl;
	cout << "Now value of *numberPtrPtr is " << *numberPtrPtr << endl;
	cout << "Now ADDRESS of numberPtrPtr (&numberPtrPtr) is " << &numberPtrPtr<< endl;
	cout << "Now value of numberPtr is " << numberPtr << endl;
	cout << "Now address of numberPtr (&numberPtr) is " << &numberPtr << endl << endl;

	// cout << "Value of &&numberPtrPtr is " << &&numberPtrPtr<< endl;
	cout << "Value of **numberPtrPtr is " << **numberPtrPtr<< endl;
	cout << "Value of &*numberPtrPtr is " << &*numberPtrPtr<< endl;
	cout << "Value of *&numberPtrPtr is " << *&numberPtrPtr<< endl;
	// cout << "Value of ***numberPtrPtr is " << ***numberPtrPtr<< endl;
	// cout << "Value of &&&numberPtrPtr is " << &&&numberPtrPtr<< endl;
	cout << "Value of &**numberPtrPtr is " << &**numberPtrPtr<< endl;
	cout << "Value of *&*numberPtrPtr is " << *&*numberPtrPtr<< endl;
	cout << "Value of **&numberPtrPtr is " << **&numberPtrPtr<< endl;
	cout << "Value of &*&numberPtrPtr is " << &*&numberPtrPtr<< endl;
	// cout << "Value of *&&numberPtrPtr is " << *&&numberPtrPtr<< endl;
	// cout << "Value of &&*numberPtrPtr is " << &&*numberPtrPtr<< endl;
	cout << endl;

	//int *** numberPtrPtrPtr;
	//numberPtrPtrPtr = &numberPtrPtr;
	//cout << LINE << "int *** numberPtrPtrPtr; " << endl;
	//cout <<  "numberPtrPtrPtr = &numberPtrPtr; " << LINE;
	//cout << "Now value of *numberPtrPtrPtr is " << *numberPtrPtrPtr << endl;
	//cout << "Now value of numberPtrPtrPtr is " << numberPtrPtrPtr << endl;
	//cout << "Now ADDRESS of numberPtrPtrPtr (&numberPtrPtrPtr) is " << &numberPtrPtrPtr<< endl;
	//cout << "Now value of numberPtrPtr is " << numberPtrPtr << endl;
	//cout << "Now address of numberPtrPtr (&numberPtrPtr) is " << &numberPtrPtr << endl;
	//cout << endl;

	numberPtr = new int;
	cout << LINE << "numberPtr = new int; " << LINE;
	cout << "Now value of numberPtr is " << numberPtr << endl;
	cout << "Now value of *numberPtr is " << *numberPtr << endl;
	cout << "Now ADDRESS of numberPtr (&numberPtr) is " << &numberPtr<< endl;
	cout << "Now value of number is " << number << endl;
	cout << "Now address of number (&number) is " << &number << endl;
	cout << endl;

	*numberPtr = NULL;
	cout << LINE << "*numberPtr = NULL; " << LINE;
	cout << "Now value of numberPtr is " << numberPtr << endl;
	cout << "Now value of *numberPtr is " << *numberPtr << endl;
	cout << "Now ADDRESS of numberPtr (&numberPtr) is " << &numberPtr<< endl;
	cout << "Now value of number is " << number << endl;
	cout << "Now address of number (&number) is " << &number << endl;
	cout << endl;

	*numberPtr = 100;
	cout << LINE << "*numberPtr = 100; " << LINE;
	cout << "Now value of numberPtr is " << numberPtr << endl;
	cout << "Now value of *numberPtr is " << *numberPtr << endl;
	cout << "Now ADDRESS of numberPtr (&numberPtr) is " << &numberPtr<< endl;
	cout << "Now value of number is " << number << endl;
	cout << "Now address of number (&number) is " << &number << endl;
	cout << endl;

	return 0;
}
