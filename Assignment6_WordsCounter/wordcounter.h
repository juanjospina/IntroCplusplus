#ifndef WORDCOUNTER_H
#define WORDCOUNTER_H
#include <iostream>
#include <string>
using namespace std;
// *******************************************************************
// * Class WordCounter:   Allows the user to create a WordCounter    *
// *    that catalogs the words and prints out summary of total      *
// *    words and total unique words in alphabetical order.          *
// *******************************************************************
class WordCounter {

public:

  struct WordStruct {
    int Counter;
    string Wordobject;
    struct WordStruct * Prev;
    struct WordStruct * Next;
  };
  WordCounter();                    // Constructor
  void Addword(const string word); // Adds a word to list
  void Printsummary();              // Prints the report


private:

  struct WordStruct * Curr;  // Points to the current record
  struct WordStruct * Head;  // The Head of the list
  struct WordStruct * Tail;  // The Tail of the list

  bool Findword(const string word);   // Finds if a word exists in the list
  string  Uppercase(string word);     // Converts the word to upper case


};

#endif
