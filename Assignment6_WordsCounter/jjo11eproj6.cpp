/***********************************************************************************************/
// Project name:    Count Words Program
// Author:		    Juan Jose Ospina Casas
// Email:			jjo11e@my.fsu.edu
// TA:			    Nan Zhao
// Course Info:	    COP3014
// Project Number:	6
// Due Date:	    07/05/2013
// Summary:		    The pogram inputs a file and check every word, while assiging and counting
//				    every word, printing out how many are uniqu words and how many times each word
//				    appears.
// Output:			Prints the summary of the input file entered, with all the information listed below.
// Assumptions:		The file must be valid, exist and be on the same folder of the program.
/***********************************************************************************************/

#include <iostream>
#include "wordcounter.h"
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

// Function Prototypes
void printProgram(ifstream &inputfile, bool check);
bool openFile(string fileused, ifstream &inputfile);

int main()
{
		// Variables
	    ifstream inputfile;		// inputfile.
		bool check;				// boolean that checks if the file is open.
		string fileused;		//string to store the name of the file.

	    do
		{

			cout << "Please enter a File name : ";
			getline(cin,fileused);

			check = openFile(fileused, inputfile);
			printProgram(inputfile, check);

		}while(fileused == "" || openFile(fileused,inputfile) == false);

		return 0;

}

// **********************************************************************************
// * Function: Print Program
// * Description: Open the file and get each of the words in the specific file, while passing
//		          them to the Addword() function by accesing it through an object of the class.
//		          Finally prints out the summary of all the words, accecsing the function Printsummary().
// * Parameter List: bool check: boolean that check if the file was correctly opened or not.
// *                 ifstream inputfile: the inputfile created to get the file.
// * Author: Juan Jose Ospina Casas
// * Date: July 5th, 2013
// **********************************************************************************
void printProgram(ifstream &inputfile, bool check)
{
		// Variables
        string string1;					//string where words of the file are stored.

        if(check == true)
        {
                WordCounter ob;			//WordCounter object.
                inputfile >> string1;

                while (!inputfile.eof() && inputfile.good())
                {
                        inputfile >> string1;
                        ob.Addword(string1);
                }

                ob.Printsummary();
        }else
        {
                cout << "File could not be opened, please enter a valid file name." << endl;
        }

}

// **********************************************************************************
// * Function: Open File
// * Description: Checks if the file input file was correctly opened (exists, or is in the right
// *              folder).
// * Parameter List: ifstream inputfile: the inputfile created that needs to be checked.
// * Author: Juan Jose Ospina Casas
// * Date: July 5th, 2013
// **********************************************************************************

bool openFile(string fileused, ifstream &inputfile)
{
        inputfile.open(fileused.c_str());

        if (inputfile.is_open())
        {
                return true;
        }else
        {
                return false;
        }

        inputfile.close();
}
