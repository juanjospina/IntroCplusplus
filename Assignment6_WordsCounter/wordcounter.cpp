#include <iostream>
#include <string>
#include <iomanip>
#include "wordcounter.h"

using namespace std;

// *********************************************************************
// * Basic Double Link List. Create a Head and tail and set them to    *
// * point to each other. Set Wordobject to low in Head and High value *
// * in the tail so all other words are inserted in between.           *
// *********************************************************************

WordCounter::WordCounter()
{
  Head = new struct WordStruct;
  Tail = new struct WordStruct;

  Head->Wordobject = "               ";   // 15 spaces, low value
  Head->Counter=1;
  Tail->Wordobject = "~~~~~~~~~~~~~~~";   // 15 ~, very high value
  Tail->Counter=1;
  Head->Prev = NULL;
  Head->Next = Tail;
  Tail->Prev = Head;
  Tail->Next = NULL;

};
// *********************************************************************
// * Member Function:  WordCounter                                     *
// * Description:  Accept a word string, check the list to see if      *
// *    exists, increment the counter if it does, make it uppercase    *
// *    and add it to the list of words in alphabetical order.         *
// * Parameter List:  const string word:  Word to be inserted.         *
// * Author: Mr. Christopher Ogden                                     *
// * Date: May 18th, 2013                                              *
// *********************************************************************

void WordCounter::Addword(const string word)
{
  string tempword = Uppercase(word);
  static struct WordStruct * NewWord;
  if (Findword(word))
    {  Curr->Counter++;
         return;
    }

  Curr = Head;
  while(tempword > Curr->Wordobject)
    Curr=Curr->Next;
  NewWord = new WordStruct;
  NewWord->Wordobject = tempword;
  NewWord->Counter = 1;

  NewWord->Next = Curr;
  NewWord->Prev = Curr->Prev;
  Curr->Prev = NewWord;
  NewWord->Prev->Next=NewWord;

}
// *********************************************************************
// * Member Function: Printsummary                                     *
// * Description:  Print all of the words, count, and summary          *
// *     information.                                                  *
// * Parameter List: None                                              *
// * Author: Mr. Christopher Ogden                                     *
// * Date: May 20th, 2013											   *
// *********************************************************************
void WordCounter::Printsummary()
{ int uniquewords=0;
 int totalwords=0;
 int remainder=0;
 int wordwidth=0;
  cout << "Word Counter Summary" << endl;
  cout << endl;
  cout << "      Word           Count" << endl;
  cout << " _______________     _____" << endl;
  cout << endl;
  Curr = Head->Next;
  while(Curr != Tail)
   { wordwidth = (15-Curr->Wordobject.length())/2;
     remainder = Curr->Wordobject.length()%2;
     for(int index=0; index<wordwidth; index++)
            cout<< ' ' ;
     cout <<Curr->Wordobject;

     cout <<setw(10+wordwidth-remainder)<<right<<Curr->Counter<<endl;
      uniquewords++;
      totalwords = totalwords+ Curr->Counter;
      Curr = Curr->Next;
   }
  cout << endl;
  cout <<"Total Number of Words: " << totalwords << endl;
  cout <<"Total Number of Unique Words: " << uniquewords << endl;
}
// *********************************************************************
// * Member Function:  FindWord                                        *
// * Description: Scans the list and checks to see if a word exists.   *
// * Parameter Description: const string word: string to be searched   *
// *        for.                                                       *
// * Author: Mr. Christopher Ogden                                     *
// * Date: May 20th, 2013											   *
// *********************************************************************
bool WordCounter::Findword(const string word)
{ Curr = Head->Next;
 string tempstr = Uppercase(word);

 while(Curr != Tail)
   {
     if(tempstr == Curr->Wordobject)  return true;
     Curr=Curr->Next;
   }
  return false;
}
// *********************************************************************
// * Member Function: Uppercase                                        *
// * Description: Makes a string upper case letters.                   *
// * Parameter Description: const string word: Word to be changed to   *
// *        uppercase.                                                 *
// * Author: Mr. Christopher Ogden                                     *
// * Date: November 1st, 20111                                         *
// *********************************************************************
string WordCounter::Uppercase(string word)
{
  int wlength = word.length();
  for (int index=0; index < wlength; index++)
       word[index] = toupper(word[index]);

  return word;
}
