/*******************************************************************/
// Project name:	Printh Month Program
// Author:			Juan Jose Ospina Casas
// Email:			jjo11e@my.fsu.edu
// TA:				Nan Zhao
// Course Info:     COP3014
// Project Number:  3
// Due Date:        05/07/2013
// Summary:			Prints out the specific month by calling the function on main and assigns
//                  the respective values from the input given.
// Input:			None by the user. Input is given in the main routine on the Main function.
// Output:			Prints out the specific month from the input given.
// Assumptions:     All inputs given in the main routine should be valid.
/*******************************************************************/

#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

const int DAYS_PER_LINE = 7;
const int FIRST_DAY=1;

void printmonth(const int month, const int startday, const bool leap);

int main(void)
{
// ******************************************************************
// * Print just a bunch of Random months - Assignment #3
// ******************************************************************
printmonth(1,0,false); // Print January 1st, on a Sunday
printmonth(2,1,true); // Print February 1st leap year, on Monday
printmonth(1,2,false); // Print January 1st, on a Tuesday
printmonth(2,3,false); // Print February 1st not leap, on Wednesday
printmonth(1,4,false); // Print January 1st, on a Thursday
printmonth(2,5,false); // Print February 1st, on a Friday
printmonth(1,6,false); // Print January 1st, on a Saturday
printmonth(6,1,false); // Print June 1st, on Monday
printmonth(12,4,false); // Print December 1st, on a Thursday*/

return 0;
}

// *****************************************************************
// * Function Name: Print Month Function.
// * Description: Print the specific month formatted properly, with the parameters
// *              given(month,startday,leap). 
// * Parameter Description: constant integer month which indicates the specific month. (1-12)
// *                        constant integer startday which indicates the specific day (Sun-Sat)(0-6)in which 1st day starts.
// *                        constant boolean leap which indicates if the year is a leap year - True - Assigns February 29 days.
// *                                                                                         - False- Assigns February 28 days.
// * Date: 05/07/2013
// * Author: Juan Jose Ospina.
// *****************************************************************

void printmonth(const int month, const int startday, const bool leap)
{
	// Array of the number of days in the respective month.
    int numberOfDays[13] = {0,31,28,31,30,31,30,31,31,30,31,30,31};

	// Array containing the names of the respective month.
    string monthName[13] = { "nothing","January", "February","March","April","May","June"
                                  ,"July","August","September","October","November","December" } ;

	// Check that the input of the month is between 1-12
    if( month>0 && month<13)
    {
       cout << setw(20)<< monthName[month] << endl;  // Print out the name of the respective month

	   // Array containing the name of the respective day of the week.
       string nameDay[7] = {" Sun "," Mon "," Tue "," Wed "," Thu "," Fri "," Sat "};

	   // Loop that prints out the names of the week (Dom-Sat)
       for(int day=0; day<7 ; day++)
       {
           cout << setw(4) << nameDay[day];
           if (day%DAYS_PER_LINE==DAYS_PER_LINE-1)
           cout << endl;
        }

	   // Check if the input is a leap year (True or False) and change 28 days to 29 days.
       if(leap==true)
       {
           numberOfDays[2] = numberOfDays[2]+1;
       }

	   // Check the given input in the variable startday and apply the specific algorithm for the starting 1st day. 
        if(startday==0)
        {
            for(int countday=1; countday<=numberOfDays[month]; countday++)
            {
                cout << setw(4) << countday <<" ";                            // prints the 1st day on Sundayn while looping
                if (countday%DAYS_PER_LINE==DAYS_PER_LINE-7)                  // for printing the rest of the month.
                    cout << endl;
            }
        }else if(startday==1)
        {
            cout<<setw(9);                                          
            cout<<FIRST_DAY<<" ";                                            // prints the 1st day on Monday
            for(int countday=2; countday<=numberOfDays[month]; countday++)  // loop that prints the rest of the month.
            {
                cout << setw(4) << countday <<" ";
                if (countday%DAYS_PER_LINE==DAYS_PER_LINE-1)
                    cout << endl;
            }

        }else if(startday==2)
        {
            cout<<setw(14);
            cout<< FIRST_DAY<<" ";                                          // prints the 1st day on Tuesday.
            for(int countday=2; countday<=numberOfDays[month]; countday++) // loop that prints the rest of the month.
            {
                cout << setw(4) << countday <<" ";
                if (countday%DAYS_PER_LINE==DAYS_PER_LINE-2)
                    cout << endl;
            }

        }else if(startday==3)
        {
			cout<<setw(19);
            cout<<FIRST_DAY<<" ";                                           // prints the 1st day on Wednesday.
            for(int countday=2; countday<=numberOfDays[month]; countday++) // loop that prints the rest of the month.
            {
                cout << setw(4) << countday <<" ";
                if (countday%DAYS_PER_LINE==DAYS_PER_LINE-3)
                    cout << endl;
            }

        }else if(startday==4)
        {
            cout<<setw(24);
            cout<<FIRST_DAY<<" ";                                           // prints the 1st day on Thursday.
            for(int countday=2; countday<=numberOfDays[month]; countday++) // loop that prints the rest of the month.
            {
                cout << setw(4) << countday <<" ";
                if (countday%DAYS_PER_LINE==DAYS_PER_LINE-4)
                    cout << endl;
            }
        }else if(startday==5)
        {
            cout<<setw(29);
            cout<<FIRST_DAY<<" ";                                           // prints the 1st day on Friday.
            for(int countday=2; countday<=numberOfDays[month]; countday++) // loop that prints the rest of the month.
            {
                cout << setw(4) << countday <<" ";
                if (countday%DAYS_PER_LINE==DAYS_PER_LINE-5)
                    cout << endl;
            }
        }else if(startday==6)
        {
            cout<<setw(34);
            cout <<FIRST_DAY<<endl;                                         // prints the 1st day on Saturday
            for(int countday=2; countday<=numberOfDays[month]; countday++) // loop that prints the rest of the month.
            {
                cout << setw(4) << countday <<" ";
                if (countday%DAYS_PER_LINE==DAYS_PER_LINE-6)
				    cout << endl;
            }
        }

    }

    cout << endl<<endl;

}

