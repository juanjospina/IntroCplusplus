/***********************************************************************************************/
// Project name:    Print Info and Draw Box Program.(Class)
// Author:		    Juan Jose Ospina Casas
// Email:			jjo11e@my.fsu.edu
// TA:			    Nan Zhao
// Course Info:	    COP3014
// Project Number:	7
// Due Date:	    12/05/2013
// Summary:		    The program displays the summary of the box specified (size, perimeter,area), and its
//					corresponding drawing.
// Output:			Prints out the summary and drawing of the box specified.
// Assumptions:		N/A
/***********************************************************************************************/

#include <iostream>
#include <iomanip>
#include <cmath>
#include "box.h"

using namespace std;

// *********************************************************************
// * Member Constructor:  Box                                     
// * Description: Creates the box, and assigns the each value received,
// *			  using the respective functions of the class.         
// * Parameter List: int sizebox = integer received by constructor(Size of the box) 
// *                 char boardbox = char received by constructor(board of the box)
// *                 char fillbox = char received by constructor(fill of the box)
// * Author: Juan Jose Ospina                                     
// * Date: July 9, 2013                                               
// *********************************************************************

box::box(int sizebox, char boardbox, char fillbox)
{
    sizeofbox =  GetSize(sizebox);
	SetBorder(boardbox);
	SetFill(fillbox);

	if(sizeofbox<1)
			sizeofbox = 1;
	if(sizeofbox>39)
			sizeofbox = 39;

}

// *********************************************************************
// * Member Function:  GetSize                                    
// * Description: Gets the size received in the constructor and assigns it to
// *			  variable realsize. Initialize the variable.       
// * Parameter List: int s : integer received.
// * Author: Juan Jose Ospina                                     
// * Date: July 9, 2013                                               
// *********************************************************************
int box::GetSize(int s)
{
    int realsize = s;
    return realsize;
}

// *********************************************************************
// * Member Function:  Perimeter                                    
// * Description: Calculates the perimeter of the box.      
// * Parameter List: N/A
// * Author: Juan Jose Ospina                                     
// * Date: July 9, 2013                                               
// *********************************************************************
int box::Perimeter()
{
    int perimeter = sizeofbox * 4;
    return perimeter;
}

// *********************************************************************
// * Member Function:  Area                                    
// * Description: Calculates the area of the box.       
// * Parameter List: N/A
// * Author: Juan Jose Ospina                                     
// * Date: July 9, 2013                                               
// *********************************************************************
int box::Area()
{
    int area = sizeofbox * sizeofbox;
    return area;
}

// *********************************************************************
// * Member Function:  Grow                                    
// * Description: Increase the size of the box by 1. Checks if by increasing,
// *              the size of the box goes out of bounds. If so, no change.
// * Parameter List: N/A
// * Author: Juan Jose Ospina                                     
// * Date: July 9, 2013                                               
// *********************************************************************
void box::Grow()
{
    if(sizeofbox >= 39)
        sizeofbox = sizeofbox;
    else
        sizeofbox = sizeofbox + 1;
}

// *********************************************************************
// * Member Function:  Shrink                                    
// * Description: Decrease the size of the box by 1. Checks if by decreasing,
// *              the size of the box goes out of bounds. If so, no change.
// * Parameter List: N/A
// * Author: Juan Jose Ospina                                     
// * Date: July 9, 2013                                               
// *********************************************************************
void box::Shrink()
{
    if(sizeofbox <= 1)
        sizeofbox = sizeofbox;
    else
        sizeofbox = sizeofbox - 1;
}


// *********************************************************************
// * Member Function:  SetBorder                                    
// * Description: Assings the respective char to the border. Sets the variable
// *              realborder to the respective char provided. If is not 
// *			  between the ASCII range(33-126), assigns the default char.
// * Parameter List: char b : char received.
// * Author: Juan Jose Ospina                                     
// * Date: July 9, 2013                                               
// *********************************************************************
void box::SetBorder(char b)
{	
	if(b>=33 && b<=126)
		realborder = b;
	else
		realborder = '#';
}

// *********************************************************************
// * Member Function:  SetFill                                    
// * Description: Assings the respective char to the fill. Sets the variable
// *              realfill to the respective char provided. If is not 
// *			  between the ASCII range(33-126), assigns the default char.
// * Parameter List: char f: char received.
// * Author: Juan Jose Ospina                                     
// * Date: July 9, 2013                                               
// *********************************************************************
void box::SetFill(char f)
{	
	if(f>=33 && f<=126)
		 realfill = f;
	else
		realfill = '*';
}


// *********************************************************************
// * Member Function:  Draw                                    
// * Description: Prints out or "Draw" the respective box, according to 
// *			  the values of the variables.
// * Parameter List: N/A
// * Author: Juan Jose Ospina                                     
// * Date: July 9, 2013                                               
// *********************************************************************
void box::Draw()
{
    cout << setfill(realborder) << setw(sizeofbox+1) << "\n";

	for (int i=1; i<sizeofbox-1;i++)
		  cout << realborder << setfill(realfill) << setw(sizeofbox-1)<< realborder <<endl;

	 cout << setfill(realborder) << setw(sizeofbox+1) << "\n";
}

// *********************************************************************
// * Member Function:  Summary                                    
// * Description: Prints out the Summary of the box specified. Calls the 
// *              respective functions needed to provide the information.
// *			  Prints out - size, perimeter, area, drawing of the box.				  
// * Parameter List: N/A
// * Author: Juan Jose Ospina                                     
// * Date: July 9, 2013                                               
// *********************************************************************
void box::Summary()
{
    cout << "The box size is : " << sizeofbox << endl;
    cout << "The perimeter is : " << Perimeter() << endl;
    cout << "The area is : " << Area() << endl;

    Draw();
}
