/***********************************************************************************************/
// Project name:		Calculate Average Program											   *
// Author:			    Juan Jose Ospina Casas												   *
// Email:			    jjo11e@my.fsu.edu													   *
// TA:				    Nan Zhao															   *
// Course Info:		    COP3014																   *
// Project Number:		5																	   *
// Due Date:		    05/28/2013															   *
// Summary:				Calculates and return the average of integers numbers				   *	
//						entered by the user. Only using pointers as variables.				   *
// Output:				Prints out the Average of the numbers input by the user.			   *
// Assumptions:			Values entered must be positive integers. Negative integers will	   *
//                      terminate the program and calculate the average for the correct		   *
//						input.																   *
/***********************************************************************************************/

#include <iostream>
#include <cmath>

using namespace std;

int calculate_Average(int *numbPtr, int *gradesTotalPtr, int *numberOfEnteredPtr );

int main()
{
	// Pointers Variable Declaration - Allocation of Memory
	 int* numbPtr = new int(0);
	 int* gradesTotalPtr = new int(0);
	 int* numberOfEnteredPtr = new int(0);


	 cout << "Please enter the numbers you want to calculate Average: " << endl
		  << "Enter a negative value to stop input." << endl;
	 cin >> *numbPtr;

	 cout << "The average is: " << calculate_Average(numbPtr,gradesTotalPtr,numberOfEnteredPtr ) << endl;

	 // Dealloaction of Memory
     delete numbPtr;
	 delete gradesTotalPtr;
	 delete numberOfEnteredPtr;

	 
}


// **********************************************************************************
// * Function: Calculate Average                                                    *                               
// * Description: Calculates the Average(mean) of the numbers entered by the user.  *                                                                             
// * Parameter List: int *numbPtr:reference to pointer numbPtr.						*
//*                       (the number entered by user.)								*
// *				 int *gradesTotalPtr:reference to pointer gradesTotalPtr.		*
//*                       (the sum of all the numbers entered.)						*
// *				 int *numberOfEntered:reference to pointer numberOfEntered.	    *
// *                      (sum of how many numbers the user have entered.)			*
// * Author: Juan Jose Ospina Casas                                                 * 
// * Date: June 27th, 2013											                *     
// ********************************************************************************** 

int calculate_Average(int *numbPtr, int *gradesTotalPtr, int *numberOfEnteredPtr )
{

	while(*numbPtr >= 0)
	{

            *gradesTotalPtr = *numbPtr + *gradesTotalPtr;
            *numberOfEnteredPtr = *numberOfEnteredPtr + 1;

            cin >> *numbPtr;

    }

	return *gradesTotalPtr / *numberOfEnteredPtr;

}
