/*******************************************************************/
// Project name:	Math Functions Calculator
// Author:			Juan Jose Ospina Casas
// Email:			jjo11e@my.fsu.edu
// TA:				Nan Zhao
// Course Info:     COP3014
// Project Number:  2
// Due Date:        05/31/2013
// Summary:			Calculate the specific function(Square root,Cube, Natural Log, Inverse, Absolute Value)
//                  for the decimal number input by the user.
// Input:			2 numbers (Select Option (0-5), decimal number)
// Output:			The result of the specific calculation done on the number input by the user.
// Assumptions:     All inputs should be valid.
/*******************************************************************/


#include <iostream>
#include <cmath>

using namespace std;

// Function Prototypes
void squareRoot ();
void cubeOp();
void naturalLog();
void inverseOp();
void absValue();


int main(void)
{
    // Variable Declaration
    char optSelect;              // store the variable for the selection of the Menu.

	// Loop for the display of the Menu and the exiting of the program.
    do
    {
        // Menu Display
        cout << "      M E N U   " << endl;
        cout << "1 - Calculate Square Root \n"<< "2 - Calculate Cube \n" << "3 - Calculate Natural Logarithm \n"
             << "4 - Calculate Inverse \n" << "5 - Calculate Absolute Value \n" << "0 - Exit Program \n"
             << "----------------------------------- \n" << "Enter Menu option : " ;

        cin  >> optSelect;      // selection option menu input.

        // Menu Prompt and Display of Results (Algorithm) - Option Selection.
        switch(optSelect)
        {
		    case '1' :
            squareRoot();
            break;

            case '2':
            cubeOp();
            break;

            case '3':
            naturalLog();
            break;

            case '4':
            inverseOp();
            break;

            case '5':
            absValue();
            break;

            case '0':
            cout << "Goodbye !"<< endl;
            break;

            default:
            cout << "Please enter a valid Menu Option (0-5)" << endl;
			break;

        }

    }while(optSelect != '0');

    return 0;
}


// *****************************************************************
// * Function Name: Square Root Function.
// * Description: Calculates the square root of the input number
// * given by the user.
// * Parameter Description: None.
// * Date: 05/30/2013
// * Author: Juan Jose Ospina.
// *****************************************************************
void squareRoot()
{
    double inputNumber;                  // store the variable input buy the user.
    cout << "Please enter a non-negative decimal : ";

	//Loop that ask for the non-negative decimal.
    do
    {
        cin >> inputNumber;              // number input by the user.
        if(inputNumber>=0)
        {
	        cout << "The square root of "<< inputNumber << " is " << sqrt(inputNumber) << endl;
        }else
        {
            cout << "Error, Please enter a valid non-negative decimal : " ;
        }
    }while(inputNumber < 0);
}

// *****************************************************************
// * Function Name: Cube Function.
// * Description: Calculates the cube of the input number
// * given by the user.
// * Parameter Description: None.
// * Date: 05/30/2013
// * Author: Juan Jose Ospina.
// *****************************************************************
void cubeOp()
{
    double inputNumber;                   // store the variable input buy the user.
    cout << "Please enter a decimal : " ;
    cin >> inputNumber;					  // number input by the user.
    cout << "The cube of " << inputNumber << " is " << pow(inputNumber,3) << endl;
}

// *****************************************************************
// * Function Name: Natural Log. Function.
// * Description: Calculates the natural logarithm of the input number
// * given by the user.
// * Parameter Description: None.
// * Date: 05/30/2013
// * Author: Juan Jose Ospina.
// *****************************************************************
void naturalLog()
{
    double inputNumber;                 // store the variable input buy the user.
    cout << "Please enter a non-negative decimal : ";

	//Loop that ask for the non-negative decimal.
    do{
        cin >> inputNumber;            // number input by the user.
        if(inputNumber>=0)
        {
            cout << "The natural log of " << inputNumber << " is " << log(inputNumber) << endl;

        }else
        {
            cout << "Error, Please enter a valid non-negative decimal : ";
        }
      }while(inputNumber<0);
}

// *****************************************************************
// * Function Name: Inverse Function.
// * Description: Calculates the inverse of the input number
// * given by the user.
// * Parameter Description: None.
// * Date: 05/30/2013
// * Author: Juan Jose Ospina.
// *****************************************************************
void inverseOp()
{
    double inputNumber;                  // store the variable input buy the user.
    cout << "Please enter a decimal number : ";
    cin >> inputNumber;                  // number input by the user.
    cout << "The inverse of " << inputNumber << " is " << 1.0/inputNumber << endl;
}

// *****************************************************************
// * Function Name: Absolute Value Function.
// * Description: Calculates the absolute value of the input number
// * given by the user.
// * Parameter Description: None.
// * Date: 05/30/2013
// * Author: Juan Jose Ospina.
// *****************************************************************
void absValue()
{
    double inputNumber;                // store the variable input buy the user.
    cout << "Please enter a decimal : ";
    cin >> inputNumber;                // number input by the user.
    cout << "The absolute value of " << inputNumber << " is " << abs(inputNumber) << endl;
}
