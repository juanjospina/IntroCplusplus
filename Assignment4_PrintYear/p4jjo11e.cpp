/*******************************************************************/
// Project name:		Printh Year Program
// Author:			    Juan Jose Ospina Casas
// Email:			    jjo11e@my.fsu.edu
// TA:				    Nan Zhao
// Course Info:		    COP3014
// Project Number:		4
// Due Date:		    05/21/2013
// Summary:				Writes-out to a file "cal.dat" the yearly calendar
//						of the specific year entered by the user. It calls
//						the printyear function to achieve the objective.
// Input:				The user must input an integer value for the year 
//						greater or equal to 1582.
// Output:				Writes-out to a file the complete yearly calendar of
//						the year entered by the user.
// Assumptions:			None.
/*******************************************************************/

#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <fstream>

using namespace std;

// Constants Values
const int DAYS_PER_LINE = 7;
const int FIRST_DAY=1;

// Prototypes Functions
int enterYear();
bool isleapyear (const int year);
int dayofweek(const int day, const int month, const int year);
void printyear(const int year);
void printmonth(const int month, const int startday, const bool leap, ofstream &outputfile);



int main(void)
{
        int yearuse;

        do
        {
                yearuse = enterYear();

                if(yearuse>=1582)
                        printyear(yearuse);

        }while (yearuse<1582);

        return 0;
}

// ************************************************************************************
// * Function Name: Enter Year Funtion
// * Description: Asks the user for the 4 digit year input. It receives a string and assigns
// *              the specific type of value, having more control of what the user inputs.
// *              Returns the value of the year int.
// * Date: 05/18/2013
// * Author: Juan Jose Ospina.
// ************************************************************************************
int enterYear()
{
        int yr;
        string mystring;

        cout << "Please enter a valid year (1582 or later) :  ";
        getline(cin,mystring);
        stringstream (mystring) >> yr;
        return yr;
}

// ****************************************************************************************************
// * Function Name: Is Leap Year Function.
// * Description: Checks if the year entered by the user passed as a parameter is a leap year.
// *              It achieves the goal by checking if the year is evenly divided by 100 and 400.
// *              Returns a boolean value (True or False).
// * Parameter Description: constant integer year. The year received at enterYear() function.
// * Date: 05/18/2013
// * Author: Juan Jose Ospina.
// ****************************************************************************************************
bool isleapyear (const int year)
{
        if ( (year % 4 == 0 && year % 100 != 0) || ( year % 400 == 0))
                return true;
        else
                return false;
}

// **********************************************************************************************************************
// * Function Name: Day of Week Function
// * Description: Returns the day of the week, or value, that assigns in which day the printhmonth() function will print
// *              the first day of the month. Return value to assign the startday. The value d will be between 0 and 6.
// *              0 is Sunday, 1 is Monday, 2 is Tuesday, 3 is Wednesday, 4 is Thursday, 5 is Friday, 6 is Saturday.
// * Parameter Description: constant integer month which indicates the specific month. (1-12)
// *                        constant integer day which indicates the specific 1st specific day. In this case 1.
// *                        constant integer year which is the value year input by the user, received at enterYear() function.                                                                                   -
// * Date: 05/18/2013
// * Author: Juan Jose Ospina.
// **********************************************************************************************************************
int dayofweek(const int day, const int month, const int year)
{
        int a, y, m, d;

        a=(14-month)/12;
        y=year-a;
        m=month+12*a-2;
        d=(day+y+y/4-y/100 + y/400 + (31*m/12))%7;

        return d;
}

// **********************************************************************************************************************
// * Function Name: Print Year Function
// * Description: Writes-out all the yearly calendar of the specific year passed as parameter. It achieves its goal by 
// *              calling the other functions, [dayweek(), isleapyear()], and by doing a for-loop in order to print every
// *              month in each iteration.
// * Parameter Description: constant integer year. It receives the year input by the user in the enterYear() function.
// * Date: 05/18/2013
// * Author: Juan Jose Ospina.
// ***********************************************************************************************************************
void printyear(const int year)
 {
        ofstream outputfile;
        outputfile.open("cal.dat");

        if (outputfile.is_open())
        {
                outputfile << endl<< setw(19)<< year<<endl<<endl;

                for (int monthused=1; monthused<=12; monthused++)
                {
                        printmonth(monthused,dayofweek(1,monthused,year),isleapyear(year), outputfile);
                }

                cout << "Your file have been generated succesfully !"<<endl;
        }else
            cout << "File could not be opened" << endl;

        outputfile.close();

}


// **********************************************************************************************************************
// * Function Name: Print Month Function.
// * Description: Writes-out to a file called "cal.dat" the specific month formatted properly, with the parameters
// *              given(month,startday,leap).
// * Parameter Description: constant integer month which indicates the specific month. (1-12)
// *                        constant integer startday which indicates the specific day (Sun-Sat)(0-6)in which 1st day starts.
// *                        constant boolean leap which indicates if the year is a leap year - True - Assigns February 29 days.
// *                                                                                         - False- Assigns February 28 days.
// *                        ofstream outputfile which is the file generated by the print year function, where the file is created.
// * Date: 05/18/2013
// * Author: Juan Jose Ospina.
// ***********************************************************************************************************************
void printmonth(const int month, const int startday, const bool leap, ofstream &outputfile)
{
    
            // Array of the number of days in the respective month.
            int numberOfDays[13] = {0,31,28,31,30,31,30,31,31,30,31,30,31};

            // Array containing the names of the respective month.
            string monthName[13] = { "nothing","January", "February","March","April","May","June"
                                    ,"July","August","September","October","November","December" } ;

            outputfile << setw(20)<< monthName[month] << endl;  // Print out the name of the respective month

            // Array containing the name of the respective day of the week.
            string nameDay[7] = {" Sun "," Mon "," Tue "," Wed "," Thu "," Fri "," Sat "};

            // Loop that prints out the names of the week (Dom-Sat)
            for(int day=0; day<7 ; day++)
            {
                    outputfile << setw(4) << nameDay[day];
                    if (day%DAYS_PER_LINE==DAYS_PER_LINE-1)
                        outputfile << endl;
            }

            // Check if the input is a leap year (True or False) and change 28 days to 29 days.
            if(leap==true)
                    numberOfDays[2] = numberOfDays[2]+1;

            // Check the given input in the variable startday and apply the specific algorithm for the starting 1st day.
            switch(startday)
            {
                    case 0:
                    for(int countday=1; countday<=numberOfDays[month]; countday++)
                    {
                            outputfile << setw(4) << countday <<" ";
                            if (countday%DAYS_PER_LINE==DAYS_PER_LINE-7)
                                outputfile << endl;
                    }
                    break;

                    case 1:
                    outputfile<<setw(9);
                    outputfile<<FIRST_DAY<<" ";
                    for(int countday=2; countday<=numberOfDays[month]; countday++)
                    {
                            outputfile << setw(4) << countday <<" ";
                            if (countday%DAYS_PER_LINE==DAYS_PER_LINE-1)
                                outputfile << endl;
                    }
                    break;

                    case 2:
                    outputfile<<setw(14);
                    outputfile<< FIRST_DAY<<" ";
                    for(int countday=2; countday<=numberOfDays[month]; countday++)
                    {
                            outputfile << setw(4) << countday <<" ";
                            if (countday%DAYS_PER_LINE==DAYS_PER_LINE-2)
                                outputfile << endl;
                    }
                    break;

                    case 3:
                    outputfile<<setw(19);
                    outputfile<<FIRST_DAY<<" ";
                    for(int countday=2; countday<=numberOfDays[month]; countday++)
                    {
                            outputfile << setw(4) << countday <<" ";
                            if (countday%DAYS_PER_LINE==DAYS_PER_LINE-3)
                                outputfile << endl;
                    }
                    break;

                    case 4:
                    outputfile<<setw(24);
                    outputfile<<FIRST_DAY<<" ";
                    for(int countday=2; countday<=numberOfDays[month]; countday++)
                    {
                            outputfile << setw(4) << countday <<" ";
                            if (countday%DAYS_PER_LINE==DAYS_PER_LINE-4)
                                outputfile << endl;
                    }
                    break;

                    case 5:
                    outputfile<<setw(29);
                    outputfile<<FIRST_DAY<<" ";
                    for(int countday=2; countday<=numberOfDays[month]; countday++)
                    {
                            outputfile << setw(4) << countday <<" ";
                            if (countday%DAYS_PER_LINE==DAYS_PER_LINE-5)
                                outputfile << endl;
                    }
                    break;

                    case 6:
                    outputfile<<setw(34);
                    outputfile <<FIRST_DAY<<endl;
                    for(int countday=2; countday<=numberOfDays[month]; countday++)
                    {
                            outputfile << setw(4) << countday <<" ";
                            if (countday%DAYS_PER_LINE==DAYS_PER_LINE-6)
						         outputfile << endl;
                    }
                    break;
             }

            outputfile << endl<<endl;

}




